import React from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { Link, NavLink} from 'react-router-dom'

import Logo from '../assets/img/open-message.svg';


export default function Header() {
    return (
            <Navbar className="navbar-dark bg-primary sticky-top" collapseOnSelect expand="md">
                <Container>
                    <Navbar.Brand className="d-flex align-items-center" as={Link} to='/'>
                        <img src={Logo} height="30" alt=""/>
                        <span className="ms-2">News</span>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
                        <Nav className="align-items-center mr-auto">
                            <Nav.Link as={NavLink} to='/' exact>Home</Nav.Link>
                            <Nav.Link as={NavLink} to='/info'>Info</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>   
                </Container>
            </Navbar>
    )
}
