import {Dropdown, DropdownButton} from 'react-bootstrap';

export default function FilterPanel({
                                        filterAuthorUp, filterAuthorDown,
                                        filterTitleUp, filterTitleDown,
                                        filterTextUp, filterTextDown,
                                        filterDateUp, filterDateDown
                                    }) {
    return (
        <div className="d-flex justify-content-end mb-5">
            <DropdownButton title="filter &#8593;" className="me-3">
                <Dropdown.Item onClick={filterAuthorUp}>by author</Dropdown.Item>
                <Dropdown.Item onClick={filterTitleUp}>by title</Dropdown.Item>
                <Dropdown.Item onClick={filterTextUp}>by message</Dropdown.Item>
                <Dropdown.Item onClick={filterDateUp}>by date</Dropdown.Item>
            </DropdownButton>
            <DropdownButton title="filter &#8595;">
                <Dropdown.Item onClick={filterAuthorDown}>by author</Dropdown.Item>
                <Dropdown.Item onClick={filterTitleDown}>by title</Dropdown.Item>
                <Dropdown.Item onClick={filterTextDown}>by message</Dropdown.Item>
                <Dropdown.Item onClick={filterDateDown}>by date</Dropdown.Item>
            </DropdownButton>
        </div>
    )
}