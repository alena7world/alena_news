import React, {useState} from 'react';
import { ListGroup, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AddNewsForm from "../Components/AddNewsForm";


function News({news, removeNews, editNews, state}) {
    const [edit, setEdit] = useState({
        id: null,
        value: ''
    })

    const editSave = (value) => {
        editNews(edit.id, value)
        setEdit({
            id: null,
            value: ''
        })
    }
    if (edit.id) {
        return (
            <div id="positionForm">
                <AddNewsForm edit={edit} state={state} onSubmit={editSave}/>
            </div>
        )
    }

        return (
            <ListGroup className="d-flex flex-column">
                {news.map((newsItem, index) => (
                    <ListGroup.Item key={index} className="mb-4">
                        <div className="d-flex justify-content-between">
                            <h2 className="fs-5">{newsItem.title}</h2>
                            <div>
                                <Button
                                    type="button"
                                    onClick={() => setEdit({...newsItem})}
                                    variant="primary"
                                    style={editButtonFont}
                                    className="btn-sm me-2">&#9998;</Button>
                                <Button
                                    type="button"
                                    onClick={() => removeNews(newsItem.id)}
                                    variant="primary"
                                    className="btn-sm">&times;</Button>
                            </div>
                        </div>
                        <p>{newsItem.text}</p>
                        <span className="d-flex justify-content-between">
                            <small>posted by 
                                <Link
                                    className="text-decoration-none"
                                    to={'/users/' + newsItem.userId}>
                                    <b> {newsItem.author}</b>
                                </Link>
                            </small>
                            <small className="d-flex justify-content-end text-muted">
                                <span className="me-2"> {newsItem.date},</span>
                                <span>{newsItem.time}</span>
                            </small>
                        </span>
                    </ListGroup.Item>
                ))}
            </ListGroup>
        )
}

export default News;

const editButtonFont = {
    fontFamily: 'Arial'
};

