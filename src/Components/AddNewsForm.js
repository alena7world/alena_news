import React, {useState} from "react";
import usersNamesIds from '../reducers/usersNamesIds'

function AddNewsForm(props) {
    const [userName, setUserName] = useState(props.edit && props.edit.author || '');
    const [newsTitle, setNewsTitle] = useState(props.edit && props.edit.title || '');
    const [newsText, setNewsText] = useState(props.edit && props.edit.text || '');

    const userNameHandler = e => {
        setUserName(e.target.value);
    }
    const newsTitleHandler = e => {
        setNewsTitle(e.target.value);
    }
    const newsTextHandler = e => {
        setNewsText(e.target.value);
    }

    const userIds = usersNamesIds.map((item) => {
        if (item.name == userName) {
            return item.id;
        }
    });

    const userId = userIds.filter(el => el != undefined);

    const submitHandler = e => {
        e.preventDefault();

        props.onSubmit({
            id: Math.floor(Math.random() * 1000),
            author: userName,
            userId: userId[0],
            title: newsTitle,
            text: newsText,
            date: new Date().toLocaleDateString(),
            time: new Date().toLocaleTimeString()
        });

        setUserName('');
        setNewsTitle('');
        setNewsText('');
    }

        return (
            <form onSubmit={submitHandler} className="bg-light border rounded p-4 mt-5 mb-5">
                    <h3 className="text-center">Want to share the news?</h3>
        
                    <div className="form-floating mb-3">
                        <select
                        className="form-select"
                        value={userName}
                        onChange={userNameHandler}
                        aria-label="Floating label select example">
                            <option selected>Choose username</option>
                            {usersNamesIds.map((item) => {
                                return (<option>{item.name}</option>)
                            })}
                        </select>
                        <label for="userSelection">Who're you?</label>
                    </div>
        
                    <div className="form-floating mb-3">
                        <input 
                        type="text" 
                        className="form-control"
                        value={newsTitle}
                        onChange={newsTitleHandler}
                        placeholder="Title" />
                        <label for="newsTitle">Title</label>
                    </div>
                    
                    <div className="form-floating mb-3">
                        <textarea 
                        className="form-control"
                        value={newsText}
                        onChange={newsTextHandler}
                        placeholder="Type the news here..."
                        style={textareaStyle} />
                        <label for="newsText">Type the news here...</label>
                    </div>
        
                    <div className="d-flex justify-content-center">
                        <button className="btn btn-primary">Post</button>
                    </div>
            </form>
        );
}


export default AddNewsForm;


const textareaStyle = {
    height: 120
};



