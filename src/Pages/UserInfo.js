import usersData from "../reducers/usersData";
import {Container} from "react-bootstrap";

function UserInfo(props) {
    const id = props.match.params.id;
    const actualUser = usersData[id - 1];
    return (
        <Container className="bg-light rounded p-5">
            <h2 className="mb-3">{actualUser.name}</h2>
            <img src={actualUser.img}
                 width="150"
                 height="150"
                 alt={actualUser.name}
                 className="mt-2 mb-4 p-2 rounded-circle bg-white" />
            <p><b>age: </b>{actualUser.age}</p>
            <p><b>about me: </b>{actualUser.biography}</p>
        </Container>
    )
}

export default UserInfo;