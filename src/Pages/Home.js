import React, {useState} from "react";
import AddNewsForm from "../Components/AddNewsForm";
import FilterPanel from "../Components/FilterPanel";
import News from "../Components/News";


export default function Home() {

    const [news, setNews] = useState([]);

    const addNews = newsItem => {
        const allNews = [newsItem, ...news];
        setNews(allNews);
    }

    const editNews = (newsId, newValue) => {
        setNews(prev => prev.map(item => (item.id === newsId ? newValue : item)));
    }

    const removeNews = (id) => {
        const newNews = [...news].filter(newsItem => newsItem.id !== id);
        setNews(newNews);
    }

    const filterBy = (fieldName, ascending) => {
        const filterNews = [...news].sort(function (a, b) {
            if (a[fieldName] < b[fieldName]) {
                return ascending ? -1 : 1;
            }
            if (a[fieldName] > b[fieldName]) {
                return ascending ? 1 : -1;
            }
            return 0;
        });
        setNews(filterNews);
    }


    const filterByDate = (fieldName, ascending) => {
        const filterNews = [...news].sort(function (a, b) {
            if (a[fieldName] < b[fieldName] || a.time < b.time) {
                return ascending ? -1 : 1;
            }
            if (a[fieldName] > b[fieldName] || a.time > b.time) {
                return ascending ? 1 : -1;
            }
            return 0;
        });
        setNews(filterNews);
    }

    return (
        <div>
            <AddNewsForm onSubmit={addNews} />
            <FilterPanel
                filterAuthorUp={filterBy.bind(this, 'author', true)}
                filterAuthorDown={filterBy.bind(this, 'author', false)}
                filterTitleUp={filterBy.bind(this, 'title', true)}
                filterTitleDown={filterBy.bind(this, 'title', false)}
                filterTextUp={filterBy.bind(this, 'text', true)}
                filterTextDown={filterBy.bind(this, 'text', false)}
                filterDateUp={filterByDate.bind(this, 'date', true)}
                filterDateDown={filterByDate.bind(this, 'date', false)}/>
            <News news={news} removeNews={removeNews} editNews={editNews} />
        </div>
    )
}