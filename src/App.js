import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Header from './Components/Header';

import Home from './Pages/Home';
import Info from './Pages/Info';
import UserInfo from './Pages/UserInfo';


function App() {
    return (
      <BrowserRouter>
        <Header />
        <Container className="pt-4">
          <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/info" component={Info} />
              <Route path="/users/:id" component={UserInfo} />
          </Switch>
        </Container>
      </BrowserRouter>
    );
}

export default App;



