import usersData from './usersData';

const usersNamesIds = usersData.map(user => {
    return {name: user.name, id: user.id};
});

export default usersNamesIds;